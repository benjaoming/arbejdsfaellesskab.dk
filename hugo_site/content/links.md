---
title: "Links"
date: 2019-05-12T18:59:31+01:00
description: "Venner or sidestykker"
draft: false
menu:
  main:
    name: "Links"
    identifier: "links"
weight: 50
---

# Fællesskaber som os:

* [S-Rummet](http://s-rummet.dk/) - megasejt solidarisk og queer-feministisk kontorfællesskab på Frederiksberg
* [Overdosseringen](http://overdosseringen.dk/) - kontorkollektiv på Østerbro med super-nice værdier
* [Lurendrejeren](https://www.facebook.com/Lurendrejeren/) - værkstedsfællesskab, energiske og kreative typer, som vi delte gårdmiljø med
* [Raboga](https://da-dk.facebook.com/ravnsborggade18c/) - dygtige kunstnere og tænkere i værkstedsfællesskab i Ravnsborggade, Nørrebro
* [Skraldespanden](https://www.facebook.com/SkraldespandenCph/) - Dejligt arbejdsfællesskab i Thorsgade, Nørrebro
