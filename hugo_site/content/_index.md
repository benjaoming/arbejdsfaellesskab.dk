---
title: "Arbejdsfaellesskabet"
date: 2019-04-30T13:33:34+02:00
draft: false
---

# Arbejdsfællesskabet

<div class="alert">

📣 Vi har 2 ledige pladser 📣

📣 Vi udlejer også vores podcaststudie 📣

Skriv på info@arbejdsfaellesskab.dk for at høre yderligere

<a href="/plads/" class="button">Læs mere</a>
</div>

Vi er en medlemsbaseret forening, der ejer og udvikler vores eget arbejdsfællesskab for selvstændige og freelancere. Vi opstod i Nordvest og efter knap 5 år flyttede vi til Blågårdsgade 19 i januar 2019.

Vi sætter pris på:

* At arbejde sammen og hver for sig
* At dele hverdagsopgaver
* Fællesskab og frokost
* Flere værdier end bare penge
* Alternative arbejdsliv
* Gør-det-selv'ere af alle slags

