---
title: "Plads i fællesskabet"
date: 2019-05-11T18:59:31+01:00
description: "Bliv kontorist: Søg om medlemskab"
draft: false
menu:
  main:
    name: "Plads i fællesskabet"
    identifier: "plads"
weight: 50
---

# Bliv kontorist: Søg om medlemskab

<div class="alert">
📣 Vi har 2 ledige pladser. 📣
</div>

* Fast plads - 1893,75 kr./md (inkl. delvis moms af driftomkostninger).
* Den bedste og hyggeligste rugbrødsbaserede frokostordning + kaffeordning (billig, men ik’ med i huslejen)
* Internet, stikkontakter, toilet, belysning, potteplanter, fri print
* Adgang til køkken, kælderrum med podcaststudie og mødelokale
* Omgang med en flok forfærdeligt fine folk, som arbejder med alt muligt såsom design, cykelkort, cybersikkerhed,  humanisme, podcast mm.

Hvis du er interesseret, så kig forbi eller skriv til:
[info@arbejdsfaellesskab.dk](mailto:info@arbejdsfaellesskab.dk)

Hvis vi ikke har nogen ledige pladser, kan du også blive skrevet på en interesse-liste og få besked, når vi har.

NB: Vi søger kun kontorister, som forventer at sidde her i længere tid, og tager derfor ikke speciale-studerende, selvom vi godt kan lide dem.

## Om os

Vi er et arbejdsfællesskab for selvstændige og freelancere. Vi opstod i Nordvest og er nu efter knap 5 år flyttet her til Blågårdsgade 19 i januar 2019.

Vi er 14 medlemmer, der ejer og udvikler vores egen arbejdsplads.

Vi sætter pris på:

* At arbejde sammen og hver for sig
* At dele hverdagsopgaver
* Fællesskab og frokost
* Flere værdier end bare penge
* Alternative arbejdsliv
* Gør-det-selv'ere af alle slags

## Ansøgning

Du søger pladsen ved at komme forbi og besøge os. Hvis du stadig er interesseret, så inviteres du til en frokost med alle de andre kontorister, der kan komme. Herefter vil vi bede dig sende en bekræftelse på din ansøgning.

## Billeder

{{< foldergallery thumbdir="photos/thumbs" origdir="photos/orig" >}}

