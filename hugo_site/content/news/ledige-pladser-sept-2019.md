---
title: "Ledige Pladser"
date: 2019-08-20T15:23:20+02:00
draft: true
---

Det går fremad i Arbejdsfællesskabet målt i socioøkonomisk, menneskelig velstand
og lykke. Vi har bl.a. fået alle de vigtigste elementer
på plads i forhold til mennesker og ting; men vi mangler faktisk 2 mennesker
til 2 ledige pladser. Dvs., at man kan blive medlem fra hhv.
1/9 og 1/11. <!--more--> Læs mere om pladser i Arbejdsfællesskabet [her](/plads/)
