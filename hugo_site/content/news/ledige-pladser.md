---
title: "Ledige Pladser"
date: 2019-04-30T16:23:20+02:00
draft: true
---

Vi har 3 ledige pladser fra henholdsvis 1/7, 1/8 og 1/9. Du kan læse mere om pladser i Arbejdsfællesskabet [her](/plads/)
