---
title: "Kontakt"
date: 2019-05-11T18:59:31+01:00
description: "Besøg os eller skriv til os"
draft: false
menu:
  main:
    name: "Kontakt"
    identifier: "kontakt"
weight: 50
---

# Kontakt

Arbejdsfællesskabet<br>
Blågårdsgade 19, kl.<br>
2200 København N

CVR: 27000258

## Elektronisk Post

Email: <a href="info@arbejdsfaellesskab.dk">info@arbejdsfaellesskab.dk</a>
