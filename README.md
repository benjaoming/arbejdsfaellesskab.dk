arbejdsfaellesskab.dk
=====================

Hele dette projekt er på engelsk. Deal with it ┌༼▀̿ Ĺ̯▀̿༽┐

This project contains a `hugo_site/` folder, which is the
[Hugo](https://gohugo.io/) project for static site generation (the main
site):

    .
    └── hugo_site  # <= Hugo project
        ├── content  # <= This is where all the content lives!
        │   └── ...
        └── themes
            └── tranetheme  # <= The theme
                ├── layouts  # <= HTML templates
                ├── static  # <= CSS etc
                └── ...

Developing the Hugo site
------------------------

We use a **static main site** because we want to archive the main
conference website as easily as possible. We are using the latest Hugo
from the Debian repositories, but the version from Ubuntu 18.04 is too
old.

See below method to directly fetch 2 .deb files from a Debian mirror and
install them.

NB! If any of the links in the below commands are broken, then browse
the mirror to find a newer version of Hugo / libsass.

``` {.sourceCode .console}
HUGO_VERSION="0.63.2"
wget -O hugo.deb https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.deb
sudo dpkg -i hugo.deb
# ...or try sudo gdebi hugo.deb

# Go to project directory inside this repository
$ cd hugo_site

# Run dev server
$ hugo server
```
